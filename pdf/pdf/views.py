from django.shortcuts import render
import pyrebase
firebaseConfig = {

    "apiKey": "AIzaSyACrtDIyzmhZ2sNQynXn-Lar3ilH-8fR58",
    "authDomain": "tqdxc1991.firebaseapp.com",
    "projectId": "tqdxc1991",
    "storageBucket": "tqdxc1991.appspot.com",
     "databaseURL": "https://tqdxc1991-default-rtdb.europe-west1.firebasedatabase.app",
    "messagingSenderId": "531674975477",
    "appId": "1:531674975477:web:020bfd0c3e36f31def49bc",
    "measurementId": "G-KWB39Q5M4M"

  }


firebase = pyrebase.initialize_app(firebaseConfig)
auth = firebase.auth()
db = firebase.database()

def signIn(request):

    return render(request,"signIn.html")

def postsign(request):
    email = request.POST.get("email")
    password = request.POST.get("pass")
    try:
        user = auth.sign_in_with_email_and_password(email,password)
        
    except:
        message = "invalid credentials"
        return render(request,"signIn.html",{"msg":message})
    
    return render(request,"welcome.html",{"e":email})

def save(request):
    data = {"name": "Mortimer 'Morty' Smith"}
    db.child("memo").push(data)
    return render(request,"save.html")